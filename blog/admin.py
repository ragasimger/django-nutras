from django.contrib import admin
from blog.models import BlogComments, Post, Category, LegalPages
from django_summernote.admin import SummernoteModelAdmin

class PostAdmin(SummernoteModelAdmin):
    list_display = ['title', 'category', 'author', 'created_date', 'updated', 'featured_image',]
    summernote_fields = ('content',)
class PageAdmin(SummernoteModelAdmin):
    list_display = ['title', 'author', 'created_date', 'featured_image', 'slug']
    summernote_fields = ('content',)

class CategoryAdmin(SummernoteModelAdmin):
    summernote_fields = ('description',)

admin.site.register(LegalPages,PageAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(BlogComments)

