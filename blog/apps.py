from django.apps import AppConfig
from django_summernote.apps import DjangoSummernoteConfig

class BlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'blog'

class MediaConfig(DjangoSummernoteConfig):
    verbose_name = "Nutras Attachments"