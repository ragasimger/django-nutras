from django.db import models
from django.urls import reverse
from django.utils import timezone
from users.models import User
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver
import datetime



class PostManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(PostManager, self).filter(draft=False).filter(created_date__lte=datetime.datetime.today())

class CommentsManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(CommentsManager, self).filter(draft=False)


class Category(models.Model):
    category = models.CharField(max_length=50, default='', unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    draft = models.BooleanField(default=False)
    description = models.TextField(default="", null=True, blank=True)
    author = models.ForeignKey(User, default="", on_delete=models.CASCADE)
    objects = CommentsManager()
    
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.category

    def get_absolute_url(self):
        return reverse("category", kwargs={'slug' : self.slug})

class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=160, default='', null=False)
    category = models.ForeignKey(Category, default=None, on_delete=models.CASCADE)
    content = models.TextField(default="")
    slug = models.SlugField(max_length=200, unique=True)
    draft = models.BooleanField(default=False)
    created_date = models.DateField(null=True, auto_now_add=False)
    updated = models.DateField(blank=True, null=True)
    featured_image = models.ImageField(upload_to="img/featured_image", default="")
    author = models.ForeignKey(User, default="", on_delete=models.CASCADE)
    objects = PostManager()
    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article_detail", kwargs={
                        "cat_slug": self.category.slug, 
                        "slug": self.slug
            }
        )


class BlogComments(models.Model):
    serial_key = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.TextField()
    comment_parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    draft = models.BooleanField(default=False)
    time = models.DateField(default=datetime.datetime.today)
    objects = CommentsManager()
    # published = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
    def __str__(self):
        return self.comment[0:15] + '...' + ' by ' + self.user.first_name + ' on ' + self.post.title


class LegalPages(models.Model):
    title = models.CharField(max_length=70, default='')
    content = models.TextField(default="")
    # content = SummernoteTextField(default="")
    created_date = models.DateField(default=datetime.datetime.today)
    featured_image = models.ImageField(upload_to="img/featured_image", default="")
    author = models.ForeignKey(User, default="", on_delete=models.CASCADE)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        verbose_name = 'Page'
        verbose_name_plural = 'Pages'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("legal-pages", kwargs={
                        "legalpageslug": self.slug
            }
        )


@receiver(post_delete)
def delete_files_when_row_deleted_from_db(sender, instance, **kwargs):
    for field in sender._meta.concrete_fields:
        if isinstance(field,models.ImageField):
            instance_file_field = getattr(instance,field.name)
            delete_file_if_unused(sender,instance,field,instance_file_field)

@receiver(pre_save)
def delete_files_when_file_changed(sender,instance, **kwargs):
    if not instance.pk:
        return
    for field in sender._meta.concrete_fields:
        if isinstance(field,models.ImageField):
            try:
                instance_in_db = sender.objects.get(pk=instance.pk)
            except sender.DoesNotExist:
                return
            instance_in_db_file_field = getattr(instance_in_db,field.name)
            instance_file_field = getattr(instance,field.name)
            if instance_in_db_file_field.name != instance_file_field.name:
                delete_file_if_unused(sender,instance,field,instance_in_db_file_field)
  
def delete_file_if_unused(model,instance,field,instance_file_field):
    dynamic_field = {}
    dynamic_field[field.name] = instance_file_field.name
    other_refs_exist = model.objects.filter(**dynamic_field).exclude(pk=instance.pk).exists()
    if not other_refs_exist:
        instance_file_field.delete(False)