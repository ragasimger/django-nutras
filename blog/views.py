from django.contrib import messages
from django.db.models.query_utils import Q
from django.http.response import HttpResponse, HttpResponseRedirect
# from django.core.exceptions import DisallowedHost, ObjectDoesNotExist
# from django.db.models import query
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView
from django.http import Http404
from blog.models import BlogComments, Post, Category
from django.db.models import Count
from blog.models import LegalPages
from projectapp.forms import CreateBlogCommentsForm
import datetime

def index(request):
    return render(request,"blog/index.html")
class Index(ListView):
    # context_object_name = 'post'
    model = Post
    template_name = "blog/index.html"
    paginate_by = 9

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        post = Post.objects.filter(draft=False, category__draft=False)
        count = Post.objects.filter(draft=False, category__draft=False).count()
        context = {
            'post' : post,
            'count' : count
        }
        return context
    

class article_category(ListView):
    context_object_name = 'post'
    model = Post
    template_name = "blog/list.html"
    paginate_by = 3

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        slug = self.kwargs.get("slug")
        category = Category.objects.get(slug=slug)
        category = category.category
        if self.request.user.is_staff or self.request.user.is_superuser:
            category = get_object_or_404(Category, slug = slug)
            posts = Post.objects.filter(category = category)
        else:
            category = get_object_or_404(Category, slug = slug, draft=False)
            posts = Post.objects.filter(category = category, draft = False)
        count = Post.objects.filter(category = category, draft = False).count()
        context = {
            'category_name':category,
            'post' : posts,
            'count' : count,
        }
        return context
        

# class article_detail(DetailView):
#     # context_object_name = 'post'
#     model = Post
#     template_name = "blog/detail.html"
#     def get_object(self):
#         queryset = self.get_queryset() #Get all fields from the model
#         cat_slug = self.kwargs.get('cat_slug') # Category Slug Passed from urls.py
#         slug = self.kwargs.get('slug') # Article Slug Passed from urls.py
#         try:
#             object = queryset.get(slug=slug, category__slug = cat_slug) # Trying to get the exact post that matches the url parameters passed(from urls.py) which are category slug(cat_slug) and article slug(slug)
#             # slug(Represents the slug variable from the defined model) = slug (Params coming from the urls.py)
#             # category__slug ---> Represents the slug from ForeignKey Model(dunder method)---- category__post should return the value of post variable from the Foreign Key of category field
#             return object
#         except ObjectDoesNotExist:
#         # object = get_object_or_404(self.model, slug=slug, category__slug = cat_slug)
#             raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

    
    
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         category = Category.objects.all()
#         count = Category.objects.all().annotate(posts_count=Count('post'))
#         blog = Post.objects.all().order_by('-date')
#         # post = Post.objects.all()
#         category = zip(category,count)
#         params = {
#             'category' : category,
#             'blog': blog,
#         }
#         return params

#####Final Function Based Article Detail view
# @login_required

# from django.utils import timezone


def article_detail(request, cat_slug=None, slug=None):
    post = get_object_or_404(Post, category__slug = cat_slug, slug = slug)
    print(post.category.draft)
    if post.draft or post.created_date > datetime.datetime.now().date() or post.category.draft==True:
        if not request.user.is_staff or not request.user.is_superuser:
            raise Http404
    category = Category.objects.all()
    count = Category.objects.filter(draft=False).annotate(post_count=Count('post', filter=Q(post__draft=False)))
    comments = BlogComments.objects.filter(post=post, comment_parent=None, draft=False)
    replies = BlogComments.objects.filter(post=post, draft=False).exclude(comment_parent=None)
    comment_reply = {}
    
    for reply in replies:
        if reply.comment_parent.serial_key not in comment_reply.keys():
            comment_reply[reply.comment_parent.serial_key] = [reply]
        else:
            comment_reply[reply.comment_parent.serial_key].append(reply)

    blog = Post.objects.filter(draft=False).order_by('-created_date')
    category = zip(category,count)
    # form = CreateBlogCommentsForm
    context = {
        'post':post, 
        'category': category, 
        'blog': blog,
        'comments' : comments,
        'comment_reply' : comment_reply,
        # 'form': form
    }
    return render(request, "blog/detail.html", context)
#####Final Function Based Article Detail view

# from django.contrib.sitemaps import Sitemap

def search(request):
    pass
    query = None
    error_message = ''
    post = ''
    exceed_error = ''
    query = request.GET.get('query')
    if len(query)==0:
        return redirect('blog-home')
    else:
        if len(query)>400:
            query = query[:50] + str('...')
            exceed_error = "Your query exceeded the length limit."
            post = Post.objects.none()
        else:
            post_title = Post.objects.filter(title__icontains=query)
            post_content = Post.objects.filter(content__icontains=query)
            query = query[:30] + str('...')
            post = post_title.union(post_content)
            print("called")
        if post.count()==0:
            error_message = "Sorry, but nothing matched your search terms. Please try again with some different keywords."
            print(error_message)
    params = {
        'post': post,
        'query': query,
        'error_message': error_message,
        'exceed_error': exceed_error
    }
    return render(request, 'blog/search.html', params)



def Comments(request):
    if request.method=="POST":
        id = request.POST.get("id")
        post = get_object_or_404(Post, id= id)
        # id = request.POST.get("id")
        # post = get_object_or_404(Post, id= id)
        comment = request.POST.get("commentbyuser")
        user = request.user
        commentserial = request.POST.get('parentcommentserial')
        if commentserial=="":
            comment = BlogComments(comment=comment, user=user, post=post)
            if not request.user.is_superuser:
                comment.draft = True
            comment.save()
            messages.success(request, "Your comment has been posted successfully")
        else:
            parent = BlogComments.objects.get(serial_key=commentserial)
            comment = BlogComments(comment=comment, user=user, post=post, comment_parent=parent)
            if not request.user.is_superuser:
                comment.draft = True
            comment.save()
            messages.success(request, "Your reply has been posted successfully")
        next = request.POST.get('nextpath', '/')
        return HttpResponseRedirect(next)

        # return redirect(f"/blog/{post.category.slug}/{post.slug}")
    else:
        return HttpResponse("Method Not Allowed")
    # return redirect(request.path)



def CustomPageDetailView(request, legalpageslug=None):
    page = get_object_or_404(LegalPages, slug = legalpageslug)
    context = {
        'page':page,
    }
    return render(request, 'projectapp/page_detail.html', context)

def BlogCommentsDetail(request,pk):
    comments = BlogComments.objects.get(pk=pk)
    context = {
        'comments': comments
    }
    return render(request, 'blog/search.html', context)