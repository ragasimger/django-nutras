from django.urls import path
from blog import views
from blog.views import article_category, article_detail, Index, CustomPageDetailView

urlpatterns = [
    path('', Index.as_view(), name='blog-home'),
    
    path('comments/', views.Comments, name='comments'),
    
    path('<slug:slug>/', article_category.as_view(), name='category'),
    
    path('legal/<slug:legalpageslug>/', views.CustomPageDetailView, name='legal-pages'),
    
    path('<slug:cat_slug>/<slug:slug>/', views.article_detail, name='article_detail'),

    
]
