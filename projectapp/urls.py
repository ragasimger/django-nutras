from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from blog import views
from projectapp import views as pr_views
from django.conf.urls import url
urlpatterns = [
    path('', views.index, name="home-page"),
    path('summernote/', include('django_summernote.urls')),
    path('users/dashboard/', pr_views.staffdash, name="staff-dashboard"),
    # Staff Dashboard

    #####<<<<!-------Post Urls Starts Here------------>########

    path('users/create-post/', pr_views.CustomPostCreateView.as_view(), name="create-blog-post"),
    # Create the post
    path('users/posts/', pr_views.CustomPostView.as_view(), name="list-blog-post"),
    # List the post
    path('users/content/edit/<slug:cat_slug>/<slug:slug>/', pr_views.CustomPostEditView.as_view(), name="update"),
    # Update the post
    path('users/content/<slug:cat_slug>/<slug:slug>/delete/', pr_views.CustomPostDeleteView.as_view(), name="delete_post"),
    # Delete the post
    
    #####<<<<!-------Post Urls Ends Here------------>########


    #####<<<<!-------Page Urls Starts Here------------>########

    path('users/create-page/', pr_views.CustomPageCreateView.as_view(), name="create-page"), # Create Legal Page
    path('users/pages/', pr_views.CustomPageListView.as_view(), name="list-pages"), 
    # List Legal Pages
    path('users/content/legal/edit/<slug:legalpageslug>/', pr_views.CustomPageEditView.as_view(), name="update_legal_page"), 
    # Update Legal Pages
    path('users/content/legal/pages/<slug:legalpageslug>/delete/', pr_views.CustomPageDeleteView.as_view(), name="delete_legal_page"),
    # Delete the Legal Page

    #####<<<<!-------Page Urls Ends Here------------>########


    #####<<<<!-------Category Urls Starts Here------------>########

    path('users/create-category/', pr_views.CustomCategoryCreateView.as_view(), name="create-categories"),
    # Create a category
    path('users/categories/', pr_views.CustomCategoriesListView.as_view(), name="list-categories"),
    # List all categories
    path('users/category/edit/<slug:cat_slug>/', pr_views.CustomCategoryView.as_view(), name="update-categories"),
    # Update a category
    path('users/category/<slug:cat_slug>/delete/', pr_views.CustomCategoryDeleteView.as_view(), name="delete-categories"),
    # Delete a category

    #####<<<<!-------Category Urls Ends Here------------>########


    #####<<<<!-------Comments Urls Starts Here------------>########

    path('users/create-comments/', pr_views.CustomComentsCreateView.as_view(), name="create-comments"),

    # Create a comment
    path('users/comments/', pr_views.CustomCommentsView.as_view(), name="list-comments"),
    # List all the comments
    path('users/comments/edit/<slug:pk>/', pr_views.CustomCommentsEditView.as_view(), name="update"),
    #Update the comment
    path('users/comments/edit/<slug:pk>/delete/', pr_views.CustomCommentsDeleteView.as_view(), name="delete_comments"),
    # Delete comments

    #####<<<<!-------Comments Urls Ends Here------------>########

    path('users/create/group/', pr_views.CustomGroupCreateView.as_view(), name="create-group"),
    
    path('users/groups/', pr_views.CustomGroupListView.as_view(), name="list-group"),

    #####<<<<!-------Change user password Urls Starts Here------------>########

    path('users/change-password/', pr_views.CustomPasswordChangeView.as_view(), name="change-custom-user-password"),
    # Change user password

    #####<<<<!-------Change user password Urls Ends Here------------>########




    #####<<<<!-------Media gallery Urls Starts Here------------>########

    path('users/media-gallery/', pr_views.CustomMediaGallery.as_view(), name="list-media-gallery"),
    # List media

    path('users/media-gallery/upload/', pr_views.CustomMediaCreateView.as_view(), name="upload-media-gallery"),
    # Add media

    path('users/media-gallery/<slug:pk>/delete/', pr_views.CustomMediaDeleteView.as_view(), name="delete-media"),
    # Delete media

    #####<<<<!-------Media gallery Urls Ends Here------------>########




    #####<<<<!-------User Urls Starts Here------------>########

    path('users/list/', pr_views.AllUsersList.as_view(), name='list-users'),
    # List Users

    path('user/<int:pk>/edit/', pr_views.CustomUserEditView.as_view(), name='update-user'),
    # Update user

    path('edit/user/<int:pk>/', pr_views.ClientUserEditView.as_view(), name='client-update-user'),
    # Client user edit

    path('user/<int:pk>/delete/', pr_views.CustomUserDeleteView.as_view(), name='delete-user'),
    # Delete user

    #####<<<<!-------User Urls Ends Here------------>########



    path('blog/', include('blog.urls')),

    path('users/', include('users.urls')),

    path('user/update/profile/<int:pk>', pr_views.ProfileUpdateView.as_view(), name='update-user-profile'),

    path('search/', views.search, name='search'),
    
    path('category/create/', pr_views.CategoryCreatePopup, name = "CategoryCreate"),

    # path('trace/', include('traceapp.urls')),
    # url(r'^filer/', include('filer.urls')),
    # path('user/add/', pr_views.CustomUserCreateView.as_view(), name='add-user'),


    # url(r'^category/create', pr_views.CategoryCreatePopup, name = "CategoryCreate"),


    # url(r'^category/(?P<pk>\d+)/edit', pr_views.CategoryEditPopup, name = "CategoryEdit"),
    # url(r'^category/ajax/get_category_id', pr_views.get_category_id, name = "get_category_id"),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
