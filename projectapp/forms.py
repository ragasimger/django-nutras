from django.contrib.auth.models import Group, User
from django import forms
from django.forms import fields
from django.utils import timezone
from blog.models import BlogComments, Post, Category, LegalPages
from django_summernote.widgets import SummernoteWidget
from mediagallery.models import MediaGallery
from users.models import UserProfile


class CreatePostForm(forms.ModelForm):
    created_date = forms.DateField(
        widget=forms.widgets.SelectDateWidget(), 
        required=False,
        label="Created Date"
        )
    content = forms.CharField(widget=SummernoteWidget())
    class Meta:
        model = Post
        fields = ['title','category', 'content', 'slug', 'featured_image', 'created_date', 'author', 'draft']

class UpdatePostForm(forms.ModelForm):
    updated = forms.DateField(
        widget=forms.widgets.SelectDateWidget(), 
        required=False
        )
    content = forms.CharField(widget=SummernoteWidget())
    class Meta:
        model = Post
        fields = ['title','category', 'content', 'slug', 'featured_image', 'updated', 'author', 'draft']
class StaffUpdatePostForm(forms.ModelForm):
    updated = forms.DateField(
        widget=forms.widgets.SelectDateWidget(), 
        required=False
        )
    content = forms.CharField(widget=SummernoteWidget())
    class Meta:
        model = Post
        fields = ['title','category', 'content', 'slug', 'featured_image', 'updated']

class CreatePageForm(forms.ModelForm):
    created_date = forms.DateField(
        widget=forms.widgets.SelectDateWidget(), 
        required=False
        )
    content = forms.CharField(widget=SummernoteWidget())

    class Meta:
        model = LegalPages
        fields = ['title', 'content', 'slug', 'featured_image', 'created_date', 'author']


class CreateCategoryForm(forms.ModelForm):
    description = forms.CharField(widget=SummernoteWidget(), required=False)
    class Meta:
        model = Category
        fields = ['category', 'slug', 'description', 'author', 'draft']

class CreateBlogCommentsForm(forms.ModelForm):
    time = forms.DateTimeField(
        label='Date',
        widget=forms.widgets.DateTimeInput(attrs={'type':'datetime-local'})
    )
    class Meta:
        model = BlogComments
        fields = ['user', 'post', 'comment', 'comment_parent', 'time', 'draft']
class UpdateBlogCommentsForm(forms.ModelForm):
    class Meta:
        model = BlogComments
        fields = ['user', 'post', 'comment', 'comment_parent','draft']
class StaffUpdateBlogCommentsForm(forms.ModelForm):
    class Meta:
        model = BlogComments
        fields = ['post', 'comment',]

class MediaCreateForm(forms.ModelForm):
    class Meta:
        model = MediaGallery
        fields = '__all__'

class UserCreateForm(forms.ModelForm):
    class Meta:
        model = User
        date = forms.DateTimeInput()
        fields = [
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_active',
            'is_superuser',
            'groups',
        ]

class ClientUserCreateForm(forms.ModelForm):
    class Meta:
        model = User
        # fields = "__all__"
        fields = [
            'first_name',
            'last_name',
            'email',            
        ]

class CustomUserGroup(forms.ModelForm):
    class Meta:
        model = Group
        fields = "__all__"

class UserProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['image']