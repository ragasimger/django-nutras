from django.contrib.auth import models
from django.db.models.aggregates import Count
from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, FormView, DeleteView
from django.views.generic.edit import UpdateView
from blog.models import BlogComments, Post, Category
from projectapp.forms import ClientUserCreateForm, CreateBlogCommentsForm, CreatePostForm, CreatePageForm, CreateCategoryForm, CustomUserGroup, MediaCreateForm, StaffUpdateBlogCommentsForm, StaffUpdatePostForm, UpdateBlogCommentsForm, UpdatePostForm, UserCreateForm, UserProfileUpdateForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render
from django.contrib.auth.models import Group, User
from blog.models import LegalPages
from users.models import UserProfile
from django.contrib.auth.views import PasswordChangeView
from mediagallery.models import MediaGallery
from django.utils.decorators import method_decorator
import datetime

#Staff Dashboard
@login_required
def staffdash(request):
    if request.user.is_staff or request.user.is_superuser:
        return render(request, 'projectapp/dashboard.html')
    else:
        return redirect('blog-home')

#   <<<<<<<<<<<<<<<<<<---- CREATE VIEW STARTS--------->>>>>>>>>>>>>>>>>>>>> #
############### CREATE VIEW STARTS HERE ###############

# Post Create View
@method_decorator(login_required, name='dispatch')
class CustomPostCreateView(FormView):
    form_class = CreatePostForm
    template_name = 'projectapp/postcreate.html'
    success_url = reverse_lazy('list-blog-post')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404
    def form_valid(self, form):
        self.object = form.save()
        if not self.request.user.is_superuser:
            self.object.author = self.request.user
            self.object.draft = True
        self.object = form.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Post"
        return context
    def get_initial(self):
        return { 'created_date': datetime.date.today }

# Page Create View
@method_decorator(login_required, name='dispatch')
class CustomPageCreateView(FormView):
    model = LegalPages
    form_class = CreatePageForm
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-pages')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Page"
        return context

# Category Create view
@method_decorator(login_required, name='dispatch')
class CustomCategoryCreateView(FormView):
    model = Category
    form_class = CreateCategoryForm
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-categories')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404
    def form_valid(self, form):
        self.object = form.save()
        if not self.request.user.is_superuser:
            self.object.author = self.request.user
            self.object.draft = True
        self.object = form.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Category"
        return context

# Category Pop Up Create View
@login_required
def CategoryCreatePopup(request):
    if request.user.is_staff or request.user.is_superuser:
        form = CreateCategoryForm(request.POST or None)
        if form.is_valid():
            instance = form.save()
            return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_category");</script>' % (instance.pk, instance))
        return render(request, "projectapp/createform.html", {"form" : form})
    else:
        raise Http404

# Comment Create View
@method_decorator(login_required, name='dispatch')
class CustomComentsCreateView(FormView):
    model = BlogComments
    form_class = CreateBlogCommentsForm
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-comments')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        self.object = form.save()
        if not self.request.user.is_superuser:
            self.object.user = self.request.user
            self.object.draft = True
        self.object = form.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Comment"
        return context

# Media Create View
@method_decorator(login_required, name='dispatch')
class CustomMediaCreateView(FormView):
    model = MediaGallery
    form_class = MediaCreateForm
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-media-gallery')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        # print("Page Created ")
        self.object = form.save()
        if not self.request.user.is_superuser:
            self.object.author = self.request.user
            self.object.draft = True
        self.object = form.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Media"
        return context


# Group Create View
@method_decorator(login_required, name='dispatch')
class CustomGroupCreateView(FormView):
    model = Group
    form_class = CustomUserGroup
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-media-gallery')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        self.object = form.save()
        return super().form_valid(form)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Group"
        return context

############### CREATE VIEW ENDS HERE ###############
#   <<<<<<<<<<<<<<<<<<---- CREATE VIEW ENDS--------->>>>>>>>>>>>>>>>>>>>> #


#   <<<<<<<<<<<<<<<<<<---- EDIT VIEW STARTS--------->>>>>>>>>>>>>>>>>>>>> #
############### EDIT VIEW STARTS HERE ###############

# Post Edit View
@method_decorator(login_required, name='dispatch')
class CustomPostEditView(UpdateView):
    staff_form_class = StaffUpdatePostForm
    admin_form_class = UpdatePostForm
    context_object_name = 'post'
    model = Post
    success_url = reverse_lazy('list-blog-post')
    admin_template_name = 'projectapp/update.html'
    staff_template_name = 'projectapp/staffpostupdate.html'

    def get_form_class(self):
        if self.request.user.is_superuser:
            return self.admin_form_class
        else:
            return self.staff_form_class

    def get_template_names(self):
        if self.request.user.is_superuser:
            return self.admin_template_name
        else:
            return self.staff_template_name

    def get(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        author = self.object.author.username
        if user == author or self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        # print(self.object.content)
        author = self.object.author.username
        if user == author or self.request.user.is_superuser:
            if not self.request.user.is_superuser:
                self.object.draft = True
                self.object.save()
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    # def get_initial(self):
    #     return { 'updated': datetime.date.today }

# Page Edit View
@method_decorator(login_required, name='dispatch')
class CustomPageEditView(UpdateView):
    form_class = CreatePageForm
    context_object_name = 'post'
    model = LegalPages
    template_name = 'projectapp/createform.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            raise Http404

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('legalpageslug')
        try:
            object = queryset.get(slug=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

# Category Edit View
@method_decorator(login_required, name='dispatch')
class CustomCategoryView(UpdateView):
    form_class = CreateCategoryForm
    context_object_name = 'post'
    success_url = reverse_lazy('list-categories')
    model = Category
    template_name = 'projectapp/createform.html'

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('cat_slug')
        try:
            object = queryset.get(slug=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

# Comments Edit View
@method_decorator(login_required, name='dispatch')
class CustomCommentsEditView(UpdateView):
    context_object_name = 'post'
    success_url = reverse_lazy('list-comments')
    model = BlogComments
    template_name = 'projectapp/createform.html'
    admin_form_class = UpdateBlogCommentsForm
    staff_form_class = StaffUpdateBlogCommentsForm


    def get_form_class(self):
        if self.request.user.is_superuser:
            return self.admin_form_class
        else:
            return self.staff_form_class

    # def get_template_names(self):
    #     if self.request.user.is_superuser:
    #         return self.admin_template_name
    #     else:
    #         return self.staff_template_name

    def get(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        author = self.object.user.username
        if user == author or self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        author = self.object.user.username
        if user == author or self.request.user.is_superuser:
            self.object.draft = True
            self.object.save()
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def get_object(self):
        queryset = self.get_queryset()
        pk = self.kwargs.get('pk')
        try:
            object = queryset.get(pk=pk)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

# User Edit View
@method_decorator(login_required, name='dispatch')
class CustomUserEditView(UpdateView):
    form_class = UserCreateForm
    context_object_name = 'post'
    model = User
    template_name = 'projectapp/createform.html'
    success_url = reverse_lazy('list-users')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('pk')
        try:
            object = queryset.get(id=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

# ClientUser Edit View
@method_decorator(login_required, name='dispatch')
class ClientUserEditView(UpdateView):
    form_class = ClientUserCreateForm
    context_object_name = 'post'
    model = User
    template_name = 'projectapp/createform.html'
    admin_success_url = reverse_lazy('list-users')
    staff_success_url = reverse_lazy('profile')

    def get(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        username = self.object.username
        if user==username or self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def get_success_url(self):
        if not self.request.user.is_superuser:
            return self.staff_success_url
        else:
            return self.admin_success_url
        # return super().get_success_url()

    def post(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        username = self.object.username
        if user==username or self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('pk')
        try:
            object = queryset.get(id=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

# Password Edit View
@method_decorator(login_required, name='dispatch')
class CustomPasswordChangeView(PasswordChangeView):
    success_url = reverse_lazy('list-users')
    template_name = 'projectapp/createform.html'

@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(UpdateView):
    form_class = UserProfileUpdateForm
    model = UserProfile
    s_success_url = reverse_lazy('list-comments')
    user_success_url = reverse_lazy('profile')
    template_name = 'projectapp/createform.html'

    def get_success_url(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return self.s_success_url
        else:
            return self.user_success_url
        


    def get(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        username = self.object.user.username
        if user==username or self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        user = self.request.user.username
        self.object = self.get_object()
        username = self.object.user.username
        if user==username or self.request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")



############### EDIT VIEW ENDS HERE ###############
#   <<<<<<<<<<<<<<<<<<---- EDIT VIEW ENDS--------->>>>>>>>>>>>>>>>>>>>> #


#   <<<<<<<<<<<<<<<<<<---- LIST VIEW STARTS--------->>>>>>>>>>>>>>>>>>>>> #
############### CONTENT LIST VIEW STARTS HERE ###############

# POST LIST VIEW
@method_decorator(login_required, name='dispatch')
class CustomPostView(ListView):
    context_object_name = 'post'
    model = Post
    template_name = 'projectapp/postlist.html'
    # paginate_by = 10

    def get_queryset(self):
        author = self.request.user.username
        # post = Post.objects.active().order_by('created_date')
        if self.request.user.is_superuser:
            post = Post.objects.all().order_by('created_date')
        elif self.request.user.is_staff:
            post = Post.objects.active().filter(author__username=author, category__draft=False).order_by('created_date')
            # post = Post.objects.filter(author__username=author).order_by('created_date')
        else:
            raise Http404
        return post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)     
        context['count'] = self.get_queryset().count()
        context['create'] = "Post"
        context['created'] = "Post"
        post = Post.objects.first()
        comments = BlogComments.objects.filter(post=post, comment_parent=None, draft=False)
        context['comments'] = comments
        return context
  
# PAGE LIST VIEW
@method_decorator(login_required, name='dispatch')
class CustomPageListView(ListView):
    model = LegalPages
    template_name = 'projectapp/widgets.html'
    def get_queryset(self):
        if self.request.user.is_superuser:
            pass
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['post'] = LegalPages.objects.all().order_by('-created_date')
        context['create'] = "Page"
        context['created'] = "Page"
        return context

# Group LIST VIEW
@method_decorator(login_required, name='dispatch')
class CustomGroupListView(ListView):
    model = LegalPages
    template_name = 'projectapp/grouplist.html'

    def get_queryset(self):
        if self.request.user.is_superuser:
            pass
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['post'] = Group.objects.all()
        context['create'] = "Group"
        context['created'] = "Group"
        return context

# CATEGORY LIST VIEW
@method_decorator(login_required, name='dispatch')
class CustomCategoriesListView(ListView):
    context_object_name = 'categories'
    model = Category
    template_name = 'projectapp/categorylist.html'

    def get_queryset(self):

        if self.request.user.is_superuser or self.request.user.is_staff:
            pass
        else:
            raise Http404
        # return category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = Category.objects.all()
        count = Category.objects.all().annotate(post_count=Count('post'))
        category = zip(post,count)
        context['counts'] = Category.objects.all().count()
        context['create'] = "Categorie"
        context['created'] = "Category"
        context['category'] = category
        return context

# COMMENTS LIST VIEW
@method_decorator(login_required, name='dispatch')
class CustomCommentsView(ListView):
    context_object_name = 'post'
    model = BlogComments
    template_name = 'projectapp/commentlist.html'


    def get_queryset(self):
        author = self.request.user.username
        post = BlogComments.objects.active().order_by('-time')
        if self.request.user.is_superuser:
            post = BlogComments.objects.all().order_by('-time')
        elif self.request.user.is_staff:
            post = BlogComments.objects.filter(user__username=author)
        else:
            raise Http404
        return post


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create'] = "Comment"
        context['created'] = "Comment"
        return context

    def get_object(self):
        queryset = self.get_queryset()
        pk = self.kwargs.get('pk')
        try:
            parent = queryset.get(pk=pk)
            serial_key = parent.serial_key
            return serial_key
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

# Users List View
@method_decorator(login_required, name='dispatch')
class AllUsersList(ListView):
    context_object_name = 'post'
    model = User
    template_name = 'projectapp/tables.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile'] = UserProfile.objects.all()
        return context

# Media List View
@method_decorator(login_required, name='dispatch')
class CustomMediaGallery(ListView):
    context_object_name = 'post'
    model = MediaGallery
    template_name = 'projectapp/pages-gallery.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

############### CONTENT LIST VIEW ENDS HERE ###############
#   <<<<<<<<<<<<<<<<<<---- LIST VIEW ENDS --------->>>>>>>>>>>>>>>>>>>>> #



#   <<<<<<<<<<<<<<<<<<---- DELETE VIEW STARTS--------->>>>>>>>>>>>>>>>>>>>> #
############### DELETE VIEW STARTS HERE ###############

# User Delete View
@method_decorator(login_required, name='dispatch')
class CustomUserDeleteView(DeleteView):
    context_object_name = 'post'
    model = User
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy('list-users')

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('pk')
        try:
            object = queryset.get(id=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)
    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(CustomUserDeleteView, self).post(request, *args, **kwargs)

# Post Delete View
@method_decorator(login_required, name='dispatch')
class CustomPostDeleteView(DeleteView):
    context_object_name = 'post'
    model = Post
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy("list-blog-post")

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            if "cancel" in request.POST:
                url = self.get_success_url()
                return HttpResponseRedirect(url)
            else:
                return super(CustomPostDeleteView, self).post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

# Page Delete View
@method_decorator(login_required, name='dispatch')
class CustomPageDeleteView(DeleteView):
    context_object_name = 'post'
    model = LegalPages
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy("list-pages")

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def get_object(self):
        queryset = self.get_queryset()
        slug = self.kwargs.get('legalpageslug')
        try:
            object = queryset.get(slug=slug)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)
    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            if "cancel" in request.POST:
                url = self.get_success_url()
                return HttpResponseRedirect(url)
            else:
                return super(CustomPageDeleteView, self).post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

# Category Delete View
@method_decorator(login_required, name='dispatch')
class CustomCategoryDeleteView(DeleteView):
    context_object_name = 'post'
    model = Category
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy("list-categories")

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def get_object(self, *args, **kwargs):
        queryset = self.get_queryset()
        slug = self.kwargs.get('cat_slug')
        if self.request.user.is_superuser:
            try:
                object = queryset.get(slug=slug)
                return object
            except ObjectDoesNotExist:
                raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)
        else:
            return HttpResponse("Unauthorized access")
    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            if "cancel" in request.POST:
                url = self.get_success_url()
                return HttpResponseRedirect(url)
            else:
                return super(CustomCategoryDeleteView, self).post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

# Comments Delete View
@method_decorator(login_required, name='dispatch')
class CustomCommentsDeleteView(DeleteView):
    context_object_name = 'post'
    model = BlogComments
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy("list-comments")

    def get_object(self):
        queryset = self.get_queryset()
        pk = self.kwargs.get('pk')
        try:
            object = queryset.get(pk=pk)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

    def get(self, request, *args, **kwargs):
        author = self.request.user.username
        self.object = self.get_object()
        user = self.object.user.username
        if self.request.user.is_superuser or author==user:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")
    def post(self, request, *args, **kwargs):
        author = self.request.user.username
        self.object = self.get_object()
        user = self.object.user.username
        if self.request.user.is_superuser or author==user:
            if "cancel" in request.POST:
                url = self.get_success_url()
                return HttpResponseRedirect(url)
            else:
                return super(CustomCommentsDeleteView, self).post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

# Media Delete View
@method_decorator(login_required, name='dispatch')
class CustomMediaDeleteView(DeleteView):
    context_object_name = 'post'
    model = MediaGallery
    template_name = 'projectapp/delete.html'
    success_url = reverse_lazy("list-media-gallery")

    def get_object(self):
        queryset = self.get_queryset()
        pk = self.kwargs.get('pk')
        try:
            object = queryset.get(pk=pk)
            return object
        except ObjectDoesNotExist:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

    def post(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            if "cancel" in request.POST:
                url = self.get_success_url()
                return HttpResponseRedirect(url)
            else:
                return super(CustomMediaDeleteView, self).post(request, *args, **kwargs)
        else:
            return HttpResponse("Unauthorized access")

############### DELETE VIEW ENDS HERE ###############

#   <<<<<<<<<<<<<<<<<<---- DELETE VIEW ENDS--------->>>>>>>>>>>>>>>>>>>>> #