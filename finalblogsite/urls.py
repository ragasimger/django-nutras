from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from blog import views
from django.conf.urls import url
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('projectapp.urls')),
    
    # path('blog/', include('blog.urls')),
    # path('users/', include('users.urls')),
    # path('search/', views.search, name='search'),
    # path('summernote/', include('django_summernote.urls')),
    # path('trace/', include('traceapp.urls')),
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
