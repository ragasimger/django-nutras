from django.shortcuts import render, redirect, resolve_url
from django.views.generic import View
from users.forms import SignUpForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from users.tokens import account_activation_token
from django.contrib.auth import login, logout
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, PasswordResetView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.html import strip_tags
from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives
from django.urls import reverse


class SignUpView(View):
    form_class = SignUpForm
    template_name = 'users/register.html'
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('blog-home')
        else:
            form = self.form_class()
            return render(request, self.template_name, {'form': form})
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save()
            user.is_active = False # Deactivate account till it is confirmed
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your Nutras Account'
            message = render_to_string('users/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            template = strip_tags(message)
            user_email = user.email
            # send_mail(subject, template, settings.EMAIL_HOST_USER, [user_email], fail_silently = False,)
            email = EmailMultiAlternatives(subject,template,settings.EMAIL_HOST_USER,[user_email],)
            email.attach_alternative(message,"text/html")
            email.fail_silently = False
            email.send()
            messages.success(request, ('Please Confirm your email to complete registration.'))
            return redirect('login')
        return render(request, self.template_name, {'form': form})

class ActivateAccount(View):
    def get(self, request, uidb64, token, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.userprofile.email_confirmed = True
            user.save()
            login(request, user)           
            messages.success(request, ('Your account have been confirmed.'))
            return redirect('blog-home')
        else:
            messages.warning(request, ('The confirmation link was invalid, possibly because it has already been used.'))
            return redirect('blog-home')

@login_required
def profile(request): 
    return render(request,"users/profile.html")

def authlogout(request):
    if request.user.is_authenticated:
        messages.success(request, 
        f'''You have been logged out from "{request.user.username}" !''')
        logout(request)
    return redirect('login')

class PasswordResetExtendedView(SuccessMessageMixin,PasswordResetView):
    email_template_name = 'users/password_reset_email.html'
    subject_template_name = 'users/password_reset_subject.txt'
    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        user = User.objects.filter(email = email)
        if user:
            user = User.objects.filter(email = email, is_active = True)
            if user:
                messages.success(request, f'''Please check your email to reset password for account associated with '{email}'.''')
                return super().post(request, *args, **kwargs)
            else:
                messages.error(request, f'''Your account isn't active yet. Please confirm your account first.''')
                return redirect('password_reset')
        else:
            user = User.objects.filter(email = email, is_active = True)
            messages.error(request, f'''Account doen't exist. Please enter the registered email !''')
            return redirect('password_reset')

class CustomLoginView(LoginView):
    def get_success_url(self):
        url = self.get_redirect_url()
        if url:
            return url
        else:
            if self.request.user.is_superuser:
                return reverse("staff-dashboard") 
            else:
                staff_user = User.objects.filter(groups__name='Staff Group')
                if self.request.user in staff_user:
                    return reverse("profile")
            return url or resolve_url(settings.LOGIN_REDIRECT_URL)