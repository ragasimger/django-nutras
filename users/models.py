from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from django.db.models.signals import post_save
from django.dispatch import receiver

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    
    image = models.ImageField(
        default="default.png", 
        upload_to='profile_images',
        null=True,
        blank=True,
        editable=True)
    image_height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="131")
    image_width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="131")

    def __str__(self):
        return f'''{self.user.username}'s Profile'''
    class Meta:
        verbose_name_plural = 'User Profiles'

    def __unicode__(self):
        return "{0}".format(self.image)
    def save(self, *args, **kwargs):
        if not self.image:
            return            
        super(UserProfile, self).save(*args, **kwargs)
        image = Image.open(self.image)
        (width, height) = image.size     
        size = ( 131, 131)
        image = image.resize(size, Image.ANTIALIAS)
        image.save(self.image.path)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
    instance.userprofile.save()