from django.urls import path
from users import views as user_views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('register/', user_views.SignUpView.as_view(), name='register'),
    path('login/', 
            user_views.CustomLoginView.as_view(
                redirect_authenticated_user=True,
                template_name='users/login.html'
            ), 
                name='login'
        ),
    path('logout/', user_views.authlogout, name='logout'),

    path('profile/', user_views.profile, name='profile'),

    path('password-reset/', 
            user_views.PasswordResetExtendedView.as_view(
                    template_name='users/password_reset.html', html_email_template_name='users/password_reset_email.html'
                ), 
                    name='password_reset'
        ),

    path('password-reset-confirm/<uidb64>/<token>/', 
            auth_views.PasswordResetConfirmView.as_view(
                    template_name='users/password_reset_confirm.html'
                ), 
                    name='password_reset_confirm'
        ),
    path('password-reset/done/', 
            auth_views.PasswordResetDoneView.as_view(
                template_name='users/password_reset_done.html'
            ), 
                name='password_reset_done'
        ), 
    path('activate/<uidb64>/<token>/', user_views.ActivateAccount.as_view(), name='activate'),
]